<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5 text-center">
				<h2>Parishioner Registration</h2>
				
				<div class="py-3"></div>
				<?php 
				// get event data
				$criteria = new CDbCriteria;
				$criteria->order = 't.id DESC';
				$criteria->limit = 1;
				$q_event = Mass_m::model()->find($criteria);
				?>
				<p><?php echo $q_event->name_sunday ?> for:<br>
					<strong>
					<!-- <?php // echo date("d F Y", strtotime($q_event->date)); ?><br> -->
					 at <?php echo $q_event->chruch_name ?></strong><br>
					 <?php echo nl2br($q_event->chruch_address) ?>
				</p>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="block_middle_white_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5">

				<div class="box-form-mass">

					<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	                    // 'type'=>'horizontal',
	                    'enableAjaxValidation'=>false,
	                    'clientOptions'=>array(
	                        'validateOnSubmit'=>false,
	                    ),
	                    'htmlOptions' => array(
	                        'enctype' => 'multipart/form-data',
	                    ),
	                )); ?>
	                <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger', 'role'=>'alert')); ?>
	                <?php if(Yii::app()->user->hasFlash('success')): ?>
	                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
	                        'alerts'=>array('success'),
	                    )); ?>
	                <?php endif; ?>
					  <div class="form-group">
					    <label>name</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'name', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label>address</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'address', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label>email</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'email', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label>phone</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'phone', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label>date of birth</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'date_birth', array('class'=>'form-control datepicker', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label>nationality</label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'nationality', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>

					  <div class="form-group">
					    <label><?php echo $form->labelEx($model, 'parish_belong'); ?></label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'parish_belong', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label><?php echo $form->labelEx($model, 'length_stay_jakarta'); ?></label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'length_stay_jakarta', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  <div class="form-group">
					    <label><?php echo $form->labelEx($model, 'family_member'); ?></label>
					    <div class="pl-5">
					    	<?php echo $form->textField($model, 'family_member', array('class'=>'form-control', 'required'=>'required')); ?>
					    </div>
					  </div>
					  
					  
					  <div class="py-1"></div>
					  <div class="row">
					  	<div class="col">
					  		<div class="g-recaptcha" data-sitekey="6LcoCugUAAAAANfsivwrYsqUmIS_45qh7aIg0JQL"></div>
					  	</div>
					  	<div class="col">
					  		<div class="text-right">
					  			<button type="submit" class="btn btn-primary">Submit</button>
					  		</div>
					  	</div>
					  </div>
					<?php $this->endWidget(); ?>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
	  
	  $( ".datepicker" ).datepicker({
	  	changeMonth: true, 
	  	changeYear: true, 
	  	dateFormat: "yy-mm-dd",
	  	yearRange : '-80:+0'
	  });

});
</script>