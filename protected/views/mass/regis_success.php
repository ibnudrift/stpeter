<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5 text-center">
				
				<div class="info_texts text-center">
					<h2>Parishioner Registration</h2> 
					<div class="py-3"></div>
					<?php 
					// get event data
					$criteria = new CDbCriteria;
					$criteria->order = 't.id DESC';
					$criteria->limit = 1;
					$q_event = Mass_m::model()->find($criteria);
					?>
					<p><?php echo $q_event->name_sunday ?> for:<br>
						<strong>
						 <!-- <?php // echo date("d F Y", strtotime($q_event->date)); ?><br> -->
						 at <?php echo $q_event->chruch_name ?></strong><br>
						 <?php echo nl2br($q_event->chruch_address) ?>
					</p>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="block_middle_white_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5">

				<div class="info_texts text-center success_reg">
					<h2>Registration Success</h2>
					<div class="py-3"></div>
					<p><?php echo $model->name ?><br><?php echo $model->age ?> years old<br><?php echo $model->phone ?><br><?php echo $model->email ?><br><?php echo $model->address ?></p>
					
					<div class="py-3"></div>
					<p class="gre mb-0">
						We have recorded your registration. Thank you for your participation.
					</p>

					<p>show this qr code upon arrival</p>
					<div class="thumbs_qrcode d-block mx-auto">
						<img src="<?php echo $model->url_qrcode ?>" alt="QrCode - <?php echo $model->name ?>" class="img-fluid">
					</div>
					<div class="py-2"></div>
					<p class="grey">
						save or screen shot <br>
						if we are unable to scan your qr code, please inform the registration staff your email address.
					</p>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</section>
