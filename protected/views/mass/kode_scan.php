<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5">
		<div class="prelatife container">

			<div class="inners_text blocks_form_tres py-5 text-center">
				<h2>Verify Code</h2>
				<div class="py-0"></div>
				<p class="mb-0">
					VERIFY THE CODE TO CONFIRM<br>YOUR PRESENCE AT OUR MASS EVENT
				</p>
				<div class="py-2 my-1"></div>
				<?php if ($error_code): ?>
					<div class="alert alert-danger d-block mw480 mx-auto" role="alert">
						<ul>
							<li>Sorry, Your Code is Wrong</li>
						</ul>
					</div>
				<?php endif ?>

				<div class="inlines-form text-center d-block mx-auto">
					<form class="justify-content-center" method="POST" action="<?php echo CHtml::normalizeUrl(array('/mass/present_event')); ?>">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="text" name="MassReg[verifyCode][]" class="form-control mr-sm-2 kode" required="" maxlength="1">
					  <input type="hidden" name="MassReg[id]" value="<?php echo $data->id; ?>">
					  <input type="hidden" name="MassReg[email]" value="<?php echo $data->email; ?>">
					  <div class="clear clearfix d-block">
						  <div class="py-1"></div>
					  </div>
					  <button type="submit" class="btn btn-light code_verify">VERIFY</button>
					</form>
				</div>

				<div class="py-5"></div>
				
				<div class="bx_start_over text-center">
					<p>OR START OVER REGISTRATION</p>
					<div class="py-2"></div>
					<a href="<?php echo CHtml::normalizeUrl(array('/mass/index')); ?>" class="btn btn-light"><i class="fa fa-edit"></i> &nbsp;Parishioner Registration</a>
				</div>

				<div class="clear"></div>
			</div>

		</div>
	</div>

	<div class="clear"></div>
</section>
