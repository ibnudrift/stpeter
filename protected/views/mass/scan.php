<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5">
		<div class="prelatife container">

			<div class="inners_text blocks_form_tres py-5 text-center">
				<h2>SORRY</h2>
				<div class="py-0"></div>
				<p class="mb-0">
					WE COULD NOT RECOGNIZE YOUR QR CODE<br>
					PLEASE ENTER EMAIL ADDRESS TO CHECK
				</p>
				<div class="py-2 my-1"></div> 
				<?php if ($error): ?>
					<div class="alert alert-danger d-block mw480 mx-auto" role="alert">
						<ul>
							<li>Sorry, You not has registered</li>
						</ul>
					</div>
				<?php endif ?>

				<div class="inlines-form text-center d-block mx-auto">
					<form class="form-inline justify-content-center" method="POST" action="">
					  <input type="text" name="MassReg[email]" class="form-control mr-sm-2" required="">
					  <button type="submit" class="btn btn-light">SUBMIT</button>
					</form>
				</div>

				<div class="py-5"></div>
				
				<div class="bx_start_over text-center">
					<p>OR START OVER REGISTRATION</p>
					<div class="py-2"></div>
					<a href="<?php echo CHtml::normalizeUrl(array('/mass/index')); ?>" class="btn btn-light"><i class="fa fa-edit"></i> &nbsp;Parishioner Registration</a>
				</div>

				<div class="clear"></div>
			</div>

		</div>
	</div>

	<div class="clear"></div>
</section>
