<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5 text-center">
				
				<div class="resutls_info_top">
					<h2>Verification Success</h2>
					<div class="py-3"></div>
					<p class="mb-0"><b>
						<?php echo $data->name; ?><br>
						<?php echo $data->age; ?> years old <br>
						<?php echo $data->phone; ?> <br>
						<?php echo $data->email; ?> <br>
						<?php echo $data->address; ?>
					</p>
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="block_middle_white_mass py-5">
		<div class="prelatife container">
			<div class="inners_text py-5">

				<div class="info_texts text-center">
					<h2>Parishioner Registration</h2> 
					<div class="py-3"></div>
					<?php 
					// get event data
					$criteria = new CDbCriteria;
					$criteria->order = 't.id DESC';
					$criteria->limit = 1;
					$q_event = Mass_m::model()->find($criteria);
					?>
					<p><?php echo $q_event->name_sunday ?> for:<br>
						<strong>
						<!-- <?php // echo date("d F Y", strtotime($q_event->date)); ?><br> -->
						 at <?php echo $q_event->chruch_name ?></strong><br>
						 <?php echo nl2br($q_event->chruch_address) ?>
					</p>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</section>
