<section class="top-page-default pg-about defaults_bigstatic" style="background-image: url(<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,869, '/images/static/'. $this->setting['about_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>);">
    <div class="inners_content">
      <!-- <h3>About STPCICP</h3>
      <p>St. Peter Canisius International Catholic Parish</p> -->
      <?php echo $this->setting['about_content']; ?>
    </div>
</section>

<section class="content-2-col blocks-home sub-content-1">
    <div class="container width_less">
        <div class="inner-about-middle content-text1 text-center">
            <?php echo $this->setting['about_content1_intro']; ?>
        </div>

        <div class="lists-blocks-next-organize mt-65">
            <div class="row">
                <?php 
                $links_arr1 = array(
                            1 => array('page'=>'history', 'title'=>'History of STPCICP'),
                                 array('page'=>'visimisi', 'title'=>'Our Vision and Mission'),
                                 array('page'=>'registration'),
                              );
                ?>

                <?php for ($i=1; $i < 4; $i++) { ?>
                <div class="col-md-20">
                    <div class="items text-center">
                        <h5 class="sub-title"><?php echo $this->setting['about_content1_title_'. $i]; ?></h5>
                        <div class="pictures">
                            <?php if ($i == 3): ?>
                                <a href="<?php echo CHtml::normalizeUrl(array('/about/registration')); ?>">
                            <?php else: ?>
                            <a href="<?php echo CHtml::normalizeUrl(array('/about/detail', 'page'=>$links_arr1[$i]['page'], 'pagename'=>$links_arr1[$i]['title'] )); ?>">
                            <?php endif ?>
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(566,302, '/images/static/'. $this->setting['about_content1_image_'. $i], array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid"></a>
                        </div>
                        <div class="info">
                            <p><?php echo $this->setting['about_content1_content_'. $i]; ?></p>
                            <?php if ($i == 3): ?>
                                <a class="btn btn-danger" href="<?php echo CHtml::normalizeUrl(array('/about/registration')); ?>">
                            <?php else: ?>
                            <a class="btn btn-danger" href="<?php echo CHtml::normalizeUrl(array('/about/detail', 'page'=>$links_arr1[$i]['page'], 'pagename'=>$links_arr1[$i]['title'] )); ?>">
                            <?php endif ?>READ MORE</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>

<section class="content-3-col blocks-pages sub-content-2">
    <div class="container">
        <h3 class="text-center"><?php echo $this->setting['about_content2_intro']; ?></h3>
        <div class="lists-blocks-next-about mt-65">
            <div class="row">
                <?php 
                // $list_about = [1=>'Community Life', 'Prayer Group', 'Adult Bible Study', 'Fellowships', 'Community Service', 'Liturgy Services'];

                $links_arr2 = array(
                            1 => array('page'=>'comlife', 'title'=>'Community Life'),
                            array('page'=>'prayer', 'title'=>'Prayer Group'),
                            array('page'=>'biblestudy', 'title'=>'Adult Bible Study'),
                            array('page'=>'fellowships', 'title'=>'Fellowships'),
                            array('page'=>'comservice', 'title'=>'Community Service'),
                            array('page'=>'liturgy', 'title'=>'Liturgy Services'),
                            );
                ?>
                <?php for ($i=1; $i < 7; $i++) { ?>
                <div class="col-md-20 col-sm-30">
                    <div class="items">
                        <div class="picturesn">
                                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(448,190, '/images/static/'. $this->setting['about_content2_image_'. $i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid">
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/about/detail', 'page'=> $links_arr2[$i]['page'], 'pagename'=> $links_arr2[$i]['title'])); ?>">
                                <div class="table-out">
                                    <div class="table-in">
                                    <h5><?php echo $this->setting['about_content2_title_'. $i] ?></h5>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="clear clearfix"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>