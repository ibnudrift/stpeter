<?php
$session = new CHttpSession;
$session->open();
$login_admin = $session['login'];
?>
<div class="leftmenu">        
    <ul class="nav nav-tabs nav-stacked">
        <li class="nav-header">Navigation</li>

        <?php if ($login_admin['type'] == 'root'): ?>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Homepage') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/home')); ?>">Homepage Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">Slides</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/slidesHome/index')); ?>">Slide Pastor</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/videoMass/index')); ?>">Video Mass</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'About') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>">About Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'history', 'title'=>'History of STPCICP')); ?>">History of STPCICP</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'visimisi', 'title'=>'Our Vision and Mission')); ?>">Our Vision and Mission</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/registration')); ?>">Registration on New Members</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'comlife', 'title'=>'Community Life')); ?>">Community Life</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'prayer', 'title'=>'Prayer Group')); ?>">Prayer Group</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'biblestudy', 'title'=>'Adult Bible Study')); ?>">Adult Bible Study</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'fellowships', 'title'=>'Fellowships')); ?>">Fellowships</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'comservice', 'title'=>'Community Service')); ?>">Community Service</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/liturgy')); ?>">Liturgy Services</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Sacraments') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'sacraments', 'title'=>'Sacraments & Services')); ?>">Sacraments Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'baptism', 'title'=>'Baptism')); ?>">Baptism</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'confirmation', 'title'=>'Confirmation')); ?>">Confirmation</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'eucharist', 'title'=>'Eucharist')); ?>">Eucharist</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'marriage', 'title'=>'Marriage')); ?>">Marriage</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'anointing', 'title'=>'Anointing Of The Sick')); ?>">Anointing Of The Sick</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'penance', 'title'=>'Penance or Reconciliation')); ?>">Penance / Reconciliation</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'funerals', 'title'=>'Funerals')); ?>">Funerals</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Next Steps') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/next')); ?>">Next Steps Overview</a></li>
                <!-- <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'church', 'title'=>'Church')); ?>">Church</a></li> -->
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'family', 'title'=>'Family')); ?>">Family</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'youth', 'title'=>'Youth')); ?>">Youth</a></li>
            </ul>
        </li>
        <?php endif ?>

        <?php if ($login_admin['type'] == 'blogger'): ?>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Event & Bulletin') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">Events Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pdf/index', 'category'=> 1)); ?>">Bulletin Overview</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Missal LIst') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pdf/index', 'category'=> 2)); ?>">Missal Overview</a></li>
            </ul>
        </li>
        <?php endif; ?>

        <?php if ($login_admin['type'] == 'root'): ?>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Event & Bulletin') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">Events Overview</a></li>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pdf/index', 'category'=> 1)); ?>">Bulletin Overview</a></li>
            </ul>
        </li>
        <li class="dropdown"><a href="#"><span class="fa fa-book"></span> <?php echo Tt::t('admin', 'Missal LIst') ?></a>
            <ul>
                <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/pdf/index', 'category'=> 2)); ?>">Missal Overview</a></li>
            </ul>
        </li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'faq', 'title'=>'Faq')); ?>"><span class="fa fa-info"></span> <?php echo Tt::t('admin', 'FAQ') ?></a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>"><span class="fa fa-phone"></span> <?php echo Tt::t('admin', 'Contact Us') ?></a></li>
        <li>&nbsp;</li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/static/mass_schedule')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Kode Mass Event') ?></a></li>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mass_m/index')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Mass Schedule Events') ?></a></li>
        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/admin/massReg')); ?>"><span class="fa fa-folder"></span> <?php // echo Tt::t('admin', 'Mass List Register') ?></a></li> -->

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/index')); ?>"><span class="fa fa-gear"></span> <?php echo Tt::t('admin', 'General Setting') ?></a></li>
        <?php endif ?>

        <?php if ($login_admin['type'] == 'admin_register'): ?>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/mass_m/index')); ?>"><span class="fa fa-folder"></span> <?php echo Tt::t('admin', 'Mass Schedule Events') ?></a></li>
        <?php endif ?>

        <li><a href="<?php echo CHtml::normalizeUrl(array('/admin/home/logout')); ?>"><span class="fa fa fa-sign-out"></span> Logout</a></li>
    </ul>
</div><!--leftmenu-->
