<section class="top-page-default insides_pg pg-step defaults_bigstatic">
  <!-- <div class="container"> -->
    <div class="inners_content">
      <h3>Next Steps</h3>
    </div>
  <!-- </div> -->
</section>

<section class="content-2-col blocks-home sub-content-1">
    <div class="container width_less">
        
        <div class="lists-blocks-next-organize">
            <div class="row">
                <?php for ($i=1; $i < 4; $i++){ ?>
                <div class="col-md-20">
                    <div class="items text-center">
                        <h5 class="sub-title"><?php echo $this->setting['nextstep_content1_title_'.$i]; ?></h5>
                        <div class="pictures"><a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=> $this->setting['nextstep_content1_title_'.$i])); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(566,302, '/images/static/'. $this->setting['nextstep_content1_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid"></a></div>
                        <div class="info">
                            <p><?php echo $this->setting['nextstep_content1_content_'.$i]; ?></p>
                            <a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=> $this->setting['nextstep_content1_title_'.$i])); ?>" class="btn btn-danger">READ MORE</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>
