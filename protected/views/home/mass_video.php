<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5 blocks_whatsnews_lst">
		<div class="prelatife container">
			<div class="inners_text py-5 text-center">
				<h2>Video Mass</h2>
                <div class="py-3"></div>
                <h4><?php echo $data->nama; ?></h4>
				<div class="py-3"></div>
                <?php 
                $url1 = $data->url_video;
                parse_str( parse_url( $url1, PHP_URL_QUERY ), $mys_var );
                ?>
				<div class="d-block mx-auto outer_block_video">
                    <div class="big_video">
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo $mys_var['v'] ?>' frameborder='0' allowfullscreen></iframe></div>
                    </div>
                </div>

				</div>
				<div class="py-3"></div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</section>