<section class="content-1-col blocks-contact-detail">
    <div class="prelatife container">
        <div class="inners-content d-block mx-auto text-center">
            <h3 class="text-center"><?php echo $this->setting['contact_hero_title']; ?></h3>
            <div class="clear height-25"></div>
            <?php echo nl2br($this->setting['contact_hero_subtitle']); ?>
            <div class="clear height-20"></div>
            <h5>OFFICE</h5>
            <?php echo $this->setting['contact_hero_content']; ?>
            <p>Telephone.<br>
            <?php echo $this->setting['contact_phone']; ?></p>
            <p>Email.<br><?php echo $this->setting['email']; ?><br></p>
            <div class="clear height-25"></div>
        </div>
    </div>
</section>

<section class="content-2-col blocks-scform-contact">
    <div class="container">
        <div class="inners-content d-block mx-auto text-center">
            <?php echo $this->setting['contact_hero_content_2']; ?>
            <div class="clear height-30"></div>
            <h5>CONTACT FORM</h5>
            <div class="clear height-40"></div>

            <div class="box-form text-left content-text2 box-block-registrations">
                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    'type'=>'horizontal',
                    'enableAjaxValidation'=>false,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>false,
                    ),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                    ),
                )); ?>
                <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger', 'role'=>'alert')); ?>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
                        'alerts'=>array('success'),
                    )); ?>
                <?php endif; ?>
                  <div class="form-group">
                    <label for="Inputname">NAME</label>
                    <div class="in_input">
                      <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="birth_2">DATE OF BIRTH</label>
                    <div class="in_input">
                      <?php echo $form->textField($model, 'date', array('class'=>'form-control datepicker')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="input_3">NATIONALITY</label>
                    <div class="in_input">
                      <?php echo $form->textField($model, 'nationality', array('class'=>'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">PHONE</label>
                    <div class="in_input">
                      <?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">EMAIL</label>
                    <div class="in_input">
                      <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="d-inlines">
                    <div class="g-recaptcha" data-sitekey="6LcoCugUAAAAANfsivwrYsqUmIS_45qh7aIg0JQL"></div>
                    </div>
                    <div class="text-right float-right">
                    <button type="submit" class="btn btn-primary"></button>
                    </div>
                  </div>
              <?php $this->endWidget(); ?>
                <div class="clear"></div>
            </div>
            <!-- End form -->

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
  $( ".datepicker" ).datepicker();
} );
</script>