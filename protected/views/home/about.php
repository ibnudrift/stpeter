<div class="height-20"></div>
<section class="content-1-col content-inner1">
	<div class="container content-text">
		<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1135,10000, '/images/static/'.$this->setting['about_image'] , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-fluid">
		<div class="line-grey margin-top-30 margin-bottom-30"></div>
		<?php echo $this->setting['about_content'] ?>
	</div>
</section>