<?php
$data->data = unserialize($data->data);
?>
<section class="top_pages_product">
    <div class="prelatife container">
      <div class="inners">

        <div class="lefts_text">
          <h3 class="tops_sub">&nbsp;</h3>
          <div class="clear"></div>
          <h1><?php echo $data->category->description->name ?></h1>
          <div class="clear"></div>
        </div>

        <div class="row backgroundsn_rights">
          <div class="col-md-2">
            &nbsp;
          </div>
          <div class="col-md-10">
            <div class="pic_banner"><img src="<?php echo $this->assetBaseurl; ?>ill-heads-productsn.jpg" alt="" class="img-responsive"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </section>

  <section class="products_middle_blocks product">
    <div class="tops_grey_fitler">
      <div class="prelatife container">
        <div class="row">
          <div class="col-md-8 col-sm-8">
            <div class="texts breadcrumbs_prdDetail">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"> PRODUK </a>
              / <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$data->category_id)); ?>"><?php echo $data->category->description->name ?></a>
              / &nbsp;&nbsp;<b><?php echo $data->description->name ?></b>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="texts text-right back-product-details">
              <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$data->category_id)); ?>"><i class="fa fa-long-arrow-left"></i>&nbsp; kembali ke halaman sebelumnya</a>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </section>

  <div class="height-40"></div>

<section class="block-product-details middle-content">
  <div class="prelatife container">
    <div class="row margin">
        <div class="col-sm-6">
            <div>
                <img class="img-thumbnail big" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(600,600, '/images/product/'.$data->image , array('method' => 'resize', 'quality' => '90')) ?>">
            </div>
            <!-- <div>
                <ul class="thumbnail-product">
                    <li>
                        <img class="img-thumbnail big" src="<?php echo $this->assetBaseurl; ?>blank.png"></li>
                    <li>
                        <img class="img-thumbnail big" src="<?php echo $this->assetBaseurl; ?>blank.png"></li>
                    <li>
                        <img class="img-thumbnail big" src="<?php echo $this->assetBaseurl; ?>blank.png"></li>
                </ul>
            </div> -->
        </div>
        <div class="col-sm-6">
          <div class="description-products-detail">
              <div class="title-produk">
                  <?php echo $data->description->name ?>
              </div>
              <!-- <div class="subtitle-produk">
                 <?php echo $data->description->name ?>
              </div> -->
              <div class="blocks_prices">
                <div class="price">
                    HARGA
                </div>
                <div class="row">
                    <?php if ($data->harga_coret > $data->harga): ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="before">
                            <?php echo Cart::money($data->harga_coret) ?>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="col-md-8 col-sm-8">
                        <div class="after">
                            <?php echo Cart::money($data->harga) ?>
                        </div>
                    </div>
                </div>
              </div>
              <div class="description">
                  <?php echo $data->description->desc ?>
                  <div class="clearfix"></div>
              </div>
              <hr>
              <div class="description">
                  <p>Anda dapat membeli di toko online di bawah ini untuk kemudahan pembayaran seperti cicilan 0% dan promo menarik lainnya. Jika anda butuh bantuan lebih lanjut, silahkan hubungi kami di <b>081 650 7667</b></p>
                  <div class="olshop">
                      <p><b>BELI PRODUK DI</b></p>
                      <?php if ($data->data['url_tokopedia'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_tokopedia'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>tokopedia.png"></a>&nbsp;&nbsp;
                      <?php endif ?>
                      <?php if ($data->data['url_bukalapak'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_bukalapak'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>bukalapak.png"></a>
                      &nbsp;&nbsp;
                      <?php endif ?>
                      <?php if ($data->data['url_lazada'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_lazada'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>logo-lazada.png"></a>
                      &nbsp;&nbsp;
                      <?php endif ?>
                      <?php if ($data->data['url_blibli'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_blibli'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>logo-blibli.png"></a>
                      &nbsp;&nbsp;
                      <?php endif ?>
                      <?php if ($data->data['url_elevania'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_elevania'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>logo-elevenia.png"></a>
                      &nbsp;&nbsp;
                      <?php endif ?>
                      <?php if ($data->data['url_shopee'] != ''): ?>
                      <a class="btn btn-default" href="<?php echo $data->data['url_shopee'] ?>" title="pt. graphic control"><img src="<?php echo $this->assetBaseurl; ?>logo-shopee.png"></a>
                      <?php endif ?>
                  </div>
                  <div class="clear height-45"></div>
                  <?php if ($data->data['url_youtube'] != ''): ?>
                  <div class="olshop">
                      <p><b>VIDEO PRODUCTS</b></p>
                      <div class="clear"></div>

                      <div class="blocks-videos-youtube prelatife">
                        <a href="https://www.youtube.com/embed/<?php echo Common::getVYoutube($data->data['url_youtube']) ?>?autoplay=1" class="view_video">
                          <img src="http://img.youtube.com/vi/<?php echo Common::getVYoutube($data->data['url_youtube']) ?>/hqdefault.jpg" alt="" class="img-thumbnail">
                        </a>
                        <div class="clear height-5"></div>
                        <p><small>*) Click picture for play video</small></p>
                      </div>
                      <div class="clear"></div>
                  </div>
                  <?php endif ?>
              </div>

              <div class="clearfix"></div>
            </div>
            <div class="height-40"></div>
        </div>
        
        <div class="clear height-20"></div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <hr class="hr-red" style="background: none; border-top:0px; margin-top:0px;">
        </div>
    </div>

    <div class="row blocks_details_spec">
      <div class="col-md-12">
        <div class="title">Spesifikasi Produk <?php echo $data->category->description->name ?> "<?php echo $data->description->name ?>"</div>

        <?php echo $data->description->note; ?>
      </div>
    </div>
    <div class="clear"></div>
    </div>
</section>

<!-- other produk -->
<section class="other product">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="title">Produk lain yang perlu anda lihat...</div>
      </div>
      </div>
      <div class="row">
        <div class="col-sm-12"><hr class="up-produk"></div>
      </div>

        <!-- Start list product -->
          <div class="list_featured_products inside_list prelatife">
            <div class="row">
              <?php foreach ($product as $key => $value): ?>
                <div class="col-md-3 col-sm-4">
                    <div class="items">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>">
                          <img class="img-responsive center-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(171,171, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                        </a>
                        <div class="clear"></div>
                        <div class="capt">
                            <div class="title text-center"><?php echo $value->description->name ?></div>
                            <div class="subtitle text-center cat2"><?php echo $value->category->description->name; ?><?php // echo $value->description->subtitle ?></div>
                        </div>
                        <div class="text-center">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>" class="btn">Lebih Lanjut</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
              <?php endforeach ?>
                <div class="clear"></div>
            </div>

        </div>
        <!-- End list product -->
        <div class="padding-top-40 text-center">
            <img class="plus" src="<?php echo $this->assetBaseurl; ?>icon-plus.png"><a class="produk" href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Lihat Produk Lainya</a>
        </div>


        <div class="clear height-50"></div>
    </div>
  </div>
</section>
<!--akhir other produk -->

<script type="text/javascript">
  $(document).ready(function(){
    $('.view_video').fancybox({
      'type': 'iframe',
    })
  })
</script>