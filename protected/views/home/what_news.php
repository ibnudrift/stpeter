<section class="outers_wrapp_mass">
	
	<div class="block_tops_grey_mass py-5 blocks_whatsnews_lst">
		<div class="prelatife container">
			<div class="inners_text py-5 text-center">
				<h2>What’s New </h2>
				<div class="py-3"></div>

				<?php
					$criteria=new CDbCriteria();
					$criteria->order = 't.id DESC';
					$list_whats = Loggers::model()->findALl($criteria);
				?>

				<div class="blocks_list_whatsnews">
					<?php foreach ($list_whats as $key => $value) { ?>
					<div class="lists mb-2 px-3 py-3 text-left">
						<span class="tops_date"><?php echo ($value->type == 'mass_video')?  "Video Mass" : ucwords($value->type) ?> - <?php echo date("d F Y", strtotime($value->date_input)) ?></span>
						<div class="py-1"></div>
						<a target="_blank" href="<?php echo $value->urls ?>"><h6><?php echo ucwords($value->name) ?></h6></a>
						<div class="py-0"></div>
						<a target="_blank" class="btn btn-link btns_link_info" href="<?php echo $value->urls ?>">Click To View</a>
					</div>
					<?php } ?>

				</div>
				<div class="py-3"></div>

				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="clear"></div>
</section>