<?php 

$criteria = new CDbCriteria;
$criteria->order = 't.date_input DESC';
$criteria->addCondition('category_id = "2"');
$criteria->addCondition('status = "1"');
$pdf_missal = Pdf::model()->findAll($criteria);
?>
<section class="content-2-col blocks-home block-events-home">
    <div class="container">
        <h3 class="text-center">Weekly Missal Boards</h3>
        
        <div class="list-blocks-eventData mt-65 missal_data">
            <div class="row">
                
                <?php foreach ($pdf_missal as $key => $value){ ?>
                <div class="col-md-15">
                    <div class="items">
                        <div class="info">
                            <span class="dates"><i class="fa fa-calendar"></i> &nbsp; <?php echo date("Y-m-d", strtotime($value->date_input)); ?></span>
                            <div class="clear height-5"></div>
                            <div class="clear clearfix"></div>
                            <h4 class="title"><?php echo $value->nama ?></h4>
                        </div>
                        <div class="blocks_s-detail">
                            <a href="<?php echo Yii::app()->baseUrl . '/images/pdf/'.$value->file; ?>" target="_blank" class="btn btn-light bmores_event">View Missal &nbsp;<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="clear"></div>
        </div>


        <div class="clear height-30"></div>
    </div>
</section>


<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '.pagination-button', function() {
        $.ajax({
              method: "GET",
              url: $(this).attr('href'),
              dataType: "html"
        })
        .done(function( data ) {
            $('.bt-show-morehome').html($(data).find('.bt-show-morehome').html());
            $('.list-blocks-eventData.events-top > .row').append($(data).find('.list-blocks-eventData.events-top > .row').html());
        });
        return false;
    });

});
</script>


<style type="text/css">
    .list-blocks-eventData .items span.dates{
        font-size: 9px !important; 
        font-weight: 400 !important;
        color: #ababab !important;
    }
</style>