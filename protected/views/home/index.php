
<section class="fly-floatmissal">
    <div class="prelative container">
        <div class="ins_right">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/missal')); ?>"><img src="<?php echo $this->assetBaseurl ?>thumb-floats-stpeters_canisius.png" alt="" class="img-responsive"></a>
        </div>
    </div>
</section>

<section class="headline">
    <div class="container">

        <div class="d-block mx-auto ins-wrap">
            <div class="row">
                <div class="col-md-60">
                    <h3><?php echo $this->setting['home_content1_intro']; ?></h3>
                    <?php echo $this->setting['home_content1_content']; ?>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="blocks-home content-1-col">
    <div class="container width-980">
        <div class="logo-program d-block mx-auto">
            <a href="<?php echo Yii::app()->baseUrl; ?>"><img src="<?php echo $this->assetBaseurl ?>logo-body.svg" alt="" class="img-fluid d-block mx-auto"></a>
        </div>
        <div class="clear height-30"></div>
        
        <?php 
        $criteria=new CDbCriteria;
        $criteria->order = 'date_input DESC';
        $criteria->limit = 1;
        $mass_top = VideoMass::model()->find($criteria);

        $url1 = $mass_top->url_video;
        parse_str( parse_url( $url1, PHP_URL_QUERY ), $mys_var );
        // echo $mys_var['v'];    
        ?>
        <div class="d-block mx-auto outer_block_video">
        	<div class="big_video">
        		<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo $mys_var['v'] ?>' frameborder='0' allowfullscreen></iframe></div>
        		<div class="clear height-15"></div>
        		<div class="text-center"><span style="color: #fff; font-size: 17px; font-weight: 500;">
                <?php echo $mass_top->nama; ?>
                </span></div>
        	</div>
            <div class="height-35"></div>

            <?php 
            $criteria=new CDbCriteria;
            $criteria->order = 'date_input DESC';
            $criteria->limit = 3;
            $criteria->offset = 1;
            $mass_all = VideoMass::model()->findAll($criteria);
            ?>
            <?php if (is_array($mass_all) && count($mass_all) > 0): ?>
            <div class="row">
                <?php foreach ($mass_all as $key => $value): ?>
                <?php
                $url_{$key} = $value->url_video; 
                parse_str( parse_url( $url_{$key}, PHP_URL_QUERY ), $mys_var_{$key} );
                ?>
                <div class="col-md-20">
                    <div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo $mys_var_{$key}['v'] ?>' frameborder='0' allowfullscreen></iframe></div>
                    <div class="clear height-10"></div>
                    <div class="text-center"><span style="color: #fff; font-size: 17px; font-weight: 500;">
                    <?php echo $value->nama; ?>
                    </span></div>
                </div>
                <?php endforeach ?>
            </div>
            <?php endif ?>
        	<div class="clear"></div>
        </div>


        <div class="clear height-50"></div>
        <div class="clear height-50"></div>
        <h3><?php echo $this->setting['home_content2_intro']; ?></h3>
        <div class="clear height-45"></div>

        <div class="lists-sntext-information">
            <div class="row">
                <?php for ($i=1; $i < 4; $i++) { ?>
                <div class="col-md-20">
                    <div class="items">
                        <div class="tops">
                            <h5><?php echo $this->setting['home_content2_day'.$i.'_name']; ?></h5>
                        </div>
                        <?php echo $this->setting['home_content2_day'.$i.'_content']; ?>
                        <div class="clear clearfix"></div>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
        <div class="text-center padding-top-45">
        <p><small><?php echo $this->setting['home_content2_bottoms_content']; ?></small></p>
        <div class="clear height-10"></div>
        <div class="d-block mx-auto hide hidden"><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>" class="btn btn-light btns_defsl">See our monthly mass schedule</a></div>
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="content4-col blocks-banner-family-homes">

    <?php if ($dataSlidePastor): ?>
    <div id="carouselExampleSlidesOnly" class="carousel slide carouselFslide" data-ride="carousel" data-interval="3500">
      <div class="carousel-inner">
        <?php foreach ($dataSlidePastor as $key => $value): ?>
        <div class="carousel-item <?php if ($key == 0): ?>active<?php endif; ?>">
            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,626, '/images/slidehome/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid mx-auto picts-desktop d-none d-sm-block">
            <div class="d-block d-sm-none">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1211,621, '/images/slidehome/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid mx-auto">
            </div>
            <div class="carousel-caption boxsn-texts-inner">
                <div class="container prelatife my-auto py-4">
                    <div class="inner">
                        <div class="row">
                            <div class="col-md-60">
                                <div class="block-texts">
                                    <h3><?php echo $value->description->title ?></h3>
                                    <?php echo $value->description->content ?>
                                    <a href="http://expatcatholicparish.org/step/detail/pagename/Family" class="btn btn-dark btns_submitsl">READ ARTICLES</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach ?>

        <?php 
        /*
        <div class="carousel-item">
            <img src="<?php echo $this->assetBaseurl ?>backn-family-banners-2.jpg" alt="" class="img-fluid mx-auto picts-desktop d-none d-sm-block">
            <div class="d-block d-sm-none">
                <img src="<?php echo $this->assetBaseurl ?>backn-family-banners-2_res.jpg" alt="" class="img-fluid mx-auto">
            </div>
            <div class="carousel-caption boxsn-texts-inner">
                <div class="container prelatife my-auto py-4">
                    <div class="inner">
                        <div class="row">
                            <div class="col-md-60">
                                <div class="block-texts">
                                    <h3>Donny & Alfa</h3>
                                    <p>We welcome you and your family. We have started youth fellowship with the help of many parishioners for the last 4 years in stpcicp. We are here to respond to the needs of youth opportunities where they can be themselves, discern their gifts, and be empowered to be effective leaders and servants called by God to the body of Christ in the world. </p>
                                    <a href="http://expatcatholicparish.org/step/detail/pagename/Family" class="btn btn-dark btns_submitsl">READ ARTICLES</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>*/ ?>

      </div>
      <ol class="carousel-indicators">
        <?php foreach ($dataSlidePastor as $key => $value): ?>
        <li data-target="#carouselExampleSlidesOnly" data-slide-to="<?php echo $key ?>" <?php if ($key == 0): ?>class="active"<?php endif ?>></li>
        <?php endforeach; ?>
      </ol>
    </div>
    <?php endif; ?>

</section>

<section class="content-2-col blocks-home">
    <div class="container">
        <h3 class="text-center">Next Step</h3>
        <div class="lists-blocks-next-organize mt-65">
            <div class="row">
                <?php for ($i=1; $i < 4; $i++){ ?>
                <div class="col-md-20">
                    <div class="items text-center">
                        <h5 class="sub-title"><?php echo $this->setting['nextstep_content1_title_'.$i]; ?></h5>
                        <div class="pictures"><a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=> $this->setting['nextstep_content1_title_'.$i])); ?>">
                            <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(566,302, '/images/static/'. $this->setting['nextstep_content1_image_'.$i] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid">
                        </a></div>
                        <div class="info">
                            <p><?php echo $this->setting['nextstep_content1_content_'.$i]; ?></p>
                            <a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=> $this->setting['nextstep_content1_title_'.$i])); ?>" class="btn btn-danger">READ MORE</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>

<section class="content-3-col blocks-home">
    <div class="container">
        <h3 class="text-center"><?php echo $this->setting['home_content3_title']; ?></h3>
        <div class="d-block mx-auto w980 text-center">
            <?php echo $this->setting['home_content3_content']; ?>
        </div>
        <div class="clear height-35"></div>

        <div class="d-block mx-auto text-center blocks_lists_auto">
            <?php for ($i=1; $i < 4; $i++) { ?>
            <?php if ( $this->setting['home_content3_youtube_url_'. $i] == '' AND $this->setting['home_content3_youtube_upload_'. $i] == ''): continue; endif ?>
            <div class="lists">
                <div class="box-videos w336 d-block mx-auto">
                    <?php if ($this->setting['home_content3_youtube_url_'. $i] != ''): ?>
                        <?php 
                        $url = $this->setting['home_content3_youtube_url_'. $i];
                        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                        ?>
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo $my_array_of_vars['v'] ?>' frameborder='0' allowfullscreen></iframe></div>
                    <?php else: ?>
                        <?php 
                        $url = $this->setting['home_content3_youtube_upload_'. $i];
                        ?>
                        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container video, .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'>
                        <video controls loop autplay>
                          <source src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo $url; ?>" type="video/mp4">
                        </video>
                        </div>
                    <?php endif ?>
                    <div class="clear"></div>
                </div>
                <div class="d-block mx-auto w336">
                    <p class="text-center"><small><?php echo $this->setting['home_content3_bottoms_content_'. $i]; ?></small></p>
                </div>
            </div>
            <?php } ?>
        </div>

        <div class="clear height-20"></div>
        <div class="d-block mx-auto text-center"><a href="<?php echo CHtml::normalizeUrl(array('/about/registration')); ?>" class="btn btn-light btns_defsl">JOIN OUR COMMUNITY</a></div>
        <div class="clear height-15"></div>
    </div>
</section>

<section class="content-2-col blocks-home block-events-home">
    <div class="container">
        <h3 class="text-center">Event</h3>

        <div class="list-blocks-eventData mt-65">
            <div class="row justify-content-center">
                <?php foreach ($dataBlog as $ke_event => $val_event): ?>
                <div class="col-md-15 col-30">
                    <div class="items">
                        <div class="picture">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/eventdetail', 'id'=> $val_event->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(322,235, '/images/blog/'. $val_event->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-fluid"></a>
                        </div>
                        <div class="info prelatife">
                            <span class="catEvent"><?php echo ucwords($val_event->topik_id); ?></span>
                            <span class="dates"><?php echo date('d F Y', strtotime($val_event->date_input)); ?></span>
                            <h4 class="title"><?php echo $val_event->description->title; ?></h4>
                            <?php if ($ke_event == 0): ?>
                                <span class="inf_new">NEW</span>
                            <?php endif ?>
                        </div>
                        <div class="blocks_s-detail">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/eventdetail', 'id'=> $val_event->id)); ?>" class="btn btn-light bmores_event">Read More &nbsp;<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>

            </div>
            <div class="clear"></div>
        </div>
        <div class="clear height-45"></div>
        <div class="d-block mx-auto text-center"><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>" class="btn btn-light btns_defsl">see more events</a></div>

        <div class="clear height-50"></div>
        <div class="clear height-50"></div>
        <div class="clear height-5"></div>
        <h3 class="text-center">Bulletin Board</h3>

        <?php
        $criteria = new CDbCriteria;
        $criteria->order = 't.date_input DESC';

        $criteria->addCondition('t.category_id = "1"');
        $criteria->addCondition('t.status = "1"');

        $criteria->limit = 4;
        $bulletin = Pdf::model()->findAll($criteria);
        ?>
        <div class="list-blocks-eventData mt-65">
            <div class="row justify-content-center">
                
                <?php foreach ($bulletin as $key => $value){ ?>
                <div class="col-md-15">
                    <div class="items">
                        <div class="info">
                            <h4 class="title"><?php echo $value->nama ?></h4>
                        </div>
                        <div class="blocks_s-detail">
                            <a href="<?php echo Yii::app()->baseUrl . '/images/pdf/'.$value->file; ?>" target="_blank" class="btn btn-light bmores_event">View Bulletin &nbsp;<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="clear"></div>
        </div>
        <div class="clear height-30"></div>
        <div class="d-block mx-auto text-center"><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>" class="btn btn-light btns_defsl">see more bulletins</a></div>

        <div class="clear"></div>
    </div>
</section>

<?php /*
<!-- slider -->
<section class="slider">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <?php
                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('description.language_id = :language_id');
                    $criteria->params[':language_id'] = $this->languageID;
                    $dataSlide = Slide::model()->findAll($criteria);
                    ?>
                <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php foreach ($dataSlide as $key => $value): ?>
                            <?php if ($value->type == 1): ?>
                                <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                                    <?php if ($value->description->url != ''): ?>
                                    <a href="<?php echo $value->description->url ?>">
                                    <?php endif ?>
                                    <img class="alat" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1552,553, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <?php if ($value->description->url != ''): ?>
                                    </a>
                                    <?php endif ?>
                                </div>
                            <?php else: ?>
                                <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                                    <img class="alat" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1552,553, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <div class="carousel-capt">
                                        <h2 class="title"><?php echo nl2br($value->description->title) ?></h2>
                                        <p class="subtitle"><?php echo $value->description->content ?>
                                        </p>
                                        <?php if ($value->description->url != ''): ?>
                                        <a href="<?php echo $value->description->url ?>" class="btn"><?php echo $value->description->url_teks ?></a>
                                        <?php endif ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>

                    </div>

                    <!-- membuat panah next dan previous -->
                    <a class="left-carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right-carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- akhir slider -->

<!-- portfolio -->
<section class="portfolio">
    <div class="container">
        <div class="tz-gallery">
  
        <div class="row">
            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>gedung.png">
                        <img src="<?php echo $this->assetBaseurl; ?>how-to-built.png" alt="gedung">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>HOW GRAPHIC <br> CONTROL BUILT <br> REPUTATION IN <br> INDONESIA</h3>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="#"><p>Learn more about us</p></a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>alat2.png">
                        <img src="<?php echo $this->assetBaseurl; ?>alat2.png" alt="alat">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>THE WORLD’S <br> LEADING <br> PRODUCTS IS AT <br> OUR HANDS</h3>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="#"><p>Learn more about us</p></a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>women.png">
                        <img src="<?php echo $this->assetBaseurl; ?>women.png" alt="women">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>THE HELP YOU <br> NEED IS JUST A <br> CALL AWAY</h3><br>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="#"><p>Learn more about us</p></a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>
<!-- akhir portfolio -->


<!-- product -->
<section class="product">
    <div class="container">
        <div class="tops text-center">
            <h3 class="sub_title">Our Featured Products</h3>
        </div>

        <div class="list_featured_products">
            <div class="row">
                <?php foreach ($product as $key => $value): ?>
                <div class="col-md-4 col-sm-4">
                    <div class="items">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>">
                            <img class="img-responsive center-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(171,171, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                        </a>
                        <div class="clear"></div>
                        <div class="capt">
                            <div class="title text-center"><?php echo $value->description->name ?></div>
                            <div class="subtitle text-center cat2"><?php echo $value->category->description->name; ?><?php // echo $value->description->subtitle ?></div>
                        </div>
                        <div class="text-center">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>" class="btn">Lebih Lanjut</a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>

            <div class="padding-top-50 text-center">
                <img class="plus" src="<?php echo $this->assetBaseurl; ?>icon-plus.png"><a class="produk" href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Lihat Produk Lainya</a>
            </div>
            <div class="height-40"></div>
        </div>

        <div class="clearfix"></div>
    </div>
</section>
<!-- akhir product -->

<!-- browse -->
<section class="browse hide hidden">
    <div class="container">
        <h2 class="text-center">Browse Our Complete Products By Category</h2>
        <div class="clear height-35"></div>
        <div class="row">
              <?php
                  $criteria = new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;
                  $criteria->addCondition('t.parent_id = :parent_id');
                  $criteria->params[':parent_id'] = 0;
                  $criteria->addCondition('t.type = :type');
                  $criteria->params[':type'] = 'category';
                  $criteria->order = 'sort ASC';
                  $criteria->limit = 5;
                  $categories = PrdCategory::model()->findAll($criteria);

              ?>
              <?php foreach ($categories as $key => $value): ?>
            <div class="col-md-15">
                <div class="title"><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a></div>
                <div class="subtitle-short">
                  <?php
                      $criteria = new CDbCriteria;
                      $criteria->with = array('description');
                      $criteria->addCondition('description.language_id = :language_id');
                      $criteria->params[':language_id'] = $this->languageID;
                      $criteria->addCondition('t.type = :type');
                      $criteria->params[':type'] = 'category';
                      $criteria->addCondition('t.parent_id = :parent_id');
                      $criteria->params[':parent_id'] = $value->id;
                      $criteria->order = 'sort ASC';
                      // $criteria->limit = 5;
                      $categories2 = PrdCategory::model()->findAll($criteria);

                  ?>
                    <ul>
                        <?php foreach ($categories2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id, 'subcategory'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
              <?php endforeach ?>
        </div>
    </div>
</section>
<!-- akhir browse -->

<!-- explore -->
<section class="explore">
    <div class="container">
        
        <div class="tops_title padding-bottom-35">
            <h2 class="text-center">Explore Our Products by Industry Solutions</h2>
        </div>

        <div class="lists_explores_industry lists-default-solutions-datas home_list">
            <div class="row text-center owl-carousel owl-theme">
                    <?php foreach ($dataBrand as $key => $value): ?>
                    <div class="col-md-12">
                        <div class="items">
                            <div class="picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(365,300, '/images/brand/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
                            <div class="info">
                                <h5 class="title"><?php echo $value->description->title ?></h5>
                                <p><?php echo $value->description->content ?></p>
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'solution'=>$value->id)); ?>" class="btn btn-default btns-def-yellow">Lebih Lanjut</a>
                                <div class="clear clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
 
            </div>
            <div class="clear"></div>
        </div>

        <div class="padding-top-50 text-center">
            <img class="plus" src="<?php echo $this->assetBaseurl; ?>icon-plus.png"><a class="produk" href="<?php echo CHtml::normalizeUrl(array('/home/solution')); ?>">Lihat Industry Solutions Lainya</a>
        </div>
        </div>
        <div class="height-40"></div>
    </div>
</section>
<!-- akhir explore -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.theme.default.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/owl.carousel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav: true,
            dots: false,
            autoplay: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
        });

    });
</script>
*/ ?>