<section class="middle-content inside-page">
  
  <section class="top_pages_product">
    <div class="prelatife container">
      <div class="inners">

        <div class="lefts_text">
          <?php if ($category == null AND $_GET['solution'] == null): ?>
          <h3 class="tops_sub">Our Product By</h3>
          <div class="clear"></div>
          <h1>Graphic Control</h1>
          <?php elseif ($_GET['solution'] != null): ?>
            <h3 class="tops_sub">Product By Solutions</h3>
            <div class="clear"></div>
            <?php 
            $criteria_sol = new CDbCriteria;
            $criteria_sol->with = array('description');
            $criteria_sol->addCondition('description.language_id = :language_id');
            $criteria_sol->params[':language_id'] = $this->languageID;

            $criteria_sol->addCondition('t.id = :sid');
            $criteria_sol->params[':sid'] = $_GET['solution'];
            $dataBrand = Brand::model()->find($criteria_sol);
            ?>
            <h1><?php echo $dataBrand->description->title; ?></h1>
          <?php else: ?>
            <h3 class="tops_sub">Product By Type</h3>
            <div class="clear"></div>
            <h1><?php echo $category->description->name ?></h1>
          <?php endif ?>
          <div class="clear"></div>
        </div>

        <div class="row backgroundsn_rights">
          <div class="col-md-2 hidden-sm hidden-xs">
            &nbsp;
          </div>
          <div class="col-md-10">
            <div class="pic_banner"><img src="<?php echo $this->assetBaseurl; ?>ill-heads-productsn.jpg" alt="" class="img-responsive"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </section>
<?php
$data = $product->getData();
?>
  <section class="products_middle_blocks product">
    <div class="tops_grey_fitler">
      <div class="prelatife container">
        <form action="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" method="get" id="form-change-select">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="texts">Showing result of <?php echo $product->getTotalItemCount() ?> products</div>
          </div>
<?php
    $criteria = new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->params[':language_id'] = $this->languageID;
    $dataBrand = Brand::model()->findAll($criteria);

?>          <div class="col-md-3 col-sm-6">
              <div class="texts">INDUSTRY APPLICATION &nbsp;&nbsp;
                <select name="solution" id="" class="form-control select-change">
                  <option value="">ALL</option>
                  <?php foreach ($dataBrand as $key => $value): ?>
                  <option value="<?php echo $value->id ?>" <?php if ($value->id == $_GET['solution']): ?>selected="selected"<?php endif ?>><?php echo $value->description->title ?></option>
                  <?php endforeach ?>
                </select>
              </div>
          </div>

<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->addCondition('t.parent_id = :parent_id');
$criteria->params[':parent_id'] = 0;
$criteria->order = 'sort ASC';
$categories = PrdCategory::model()->findAll($criteria);

?>

          <div class="col-md-3 col-sm-6">
              <div class="texts">PRODUCT TYPE &nbsp;&nbsp;
                <select name="category" id="" class="form-control select-change">
                  <option value="">ALL</option>
                  <?php foreach ($categories as $key => $value): ?>
                  <option value="<?php echo $value->id ?>" <?php if ($value->id == $_GET['category']): ?>selected="selected"<?php endif ?>><?php echo $value->description->name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="texts text-right">
                <?php $this->widget('CLinkPager', array(
                    'pages' => $product->getPagination(),
                    'header'=>'',
                    'htmlOptions'=>array('class'=>'pagination'),
                    'selectedPageCssClass'=>'active',
                )) ?>
            </div>
          </div>
        </div>
        </form>
<script type="text/javascript">
$('.select-change').on('change', function() {
  $('#form-change-select').submit();
})
</script>
        <div class="clear"></div>
      </div>
    </div>

    <div class="middles">
      <div class="prelatife container">
        <div class="clear height-50"></div>
        <div class="clear height-30"></div>

        <!-- Start list product -->
          <div class="list_featured_products inside_list prelatife">
            <div class="row">
              <?php foreach ($data as $key => $value): ?>
                <div class="col-md-3 col-sm-4">
                    <div class="items">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>">
                          <img class="img-responsive center-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(171,171, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                        </a>
                        <div class="clear"></div>
                        <div class="capt">
                            <div class="title text-center"><?php echo ($value->description->name); ?></div>
                            <div class="subtitle text-center cat2"><?php echo $value->category->description->name; ?><?php // echo $value->description->subtitle ?></div>
                        </div>
                        <div class="text-center">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>" class="btn">Lebih Lanjut</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
              <?php endforeach ?>
                <div class="clear"></div>
            </div>

            <div class="padding-top-50 text-center box-pagination">
                  <!-- <span class="inline-block">PAGE</span>&nbsp; -->
                  <!-- <ul class="list-inline">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul> -->
                <?php $this->widget('CLinkPager', array(
                    'pages' => $product->getPagination(),
                    'header'=>'',
                    'htmlOptions'=>array('class'=>'pagination'),
                    'selectedPageCssClass'=>'active',
                )) ?>
                  <div class="clear"></div>
            </div>
            <div class="clear height-50"></div>
            <div class="clear height-30"></div>
        </div>
        <!-- End list product -->
        
        <div class="clear clearfix"></div>
      </div>

      <div class="clear"></div>
    </div>
  </section>

  <div class="clear"></div>
</section>