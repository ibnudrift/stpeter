<section class="top-page-default insides_pg pg_sacraments">
  <!-- <div class="container"> -->
    <div class="inners_content">
      <p>Sacraments</p>
      <div class="clear height-10"></div><div class="height-3"></div>
    <?php if ($_GET['pagename'] == 'penance - reconciliation'): ?>
        <h3><?php echo ucwords('penance / reconciliation'); ?></h3>
    <?php else: ?>
            <h3><?php echo ucwords($_GET['pagename']); ?></h3>
    <?php endif ?>
    </div>
  <!-- </div> -->
</section>

<section class="content-2-col sub-content-1 block-page-sacraments-content">
    <div class="container">
        <div class="content-text2 inners-cont d-block mx-auto">

            <?php echo $this->setting[$_GET['page'].'_hero_content']; ?>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>
    </div>
</section>



<section class="content-3-col blocks-pages sub-content-2">
    <div class="container2">
        <div class="clear height-10"></div>
        <h3 class="text-center">Our Sacraments</h3>

        <div class="lists-blocks-next-about mt-65 lists-thumb-sacraments mx-auto">
                <?php 
                // $list_about = [1=>'BAPTISM', 'CONFIRMATION', 'EUCHARISt', 'MARRIAGE', 'ANOINTING OF THE SICK', 'PENANCE / RECONCILIATION', 'FUNERALS'];
                $list_about = array(
                             1=>array('page'=> 'baptism', 'title'=>'BAPTISM'),
                                array('page'=> 'confirmation', 'title'=>'CONFIRMATION'),
                                array('page'=> 'eucharist', 'title'=>'EUCHARISt'),
                                array('page'=> 'marriage', 'title'=>'MARRIAGE'),
                                array('page'=> 'anointing', 'title'=>'ANOINTING OF THE SICK'),
                                array('page'=> 'penance', 'title'=>'PENANCE / RECONCILIATION'),
                                // array('page'=> 'funerals', 'title'=>'FUNERALS'),
                            );
                ?>
                <?php foreach ($list_about as $key => $value): ?>
                    <div class="items">
                        <div class="picturesn">
                                <img src="<?php echo $this->assetBaseurl ?>sn-thumb-pict-sacraments-<?php echo $key ?>.jpg" alt="" class="img-fluid">
                            <?php 
                            $stn_name = str_replace('/', '-', $value['title']);
                            ?>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/sacrament/detail', 'page'=>$value['page'], 'pagename'=>strtolower($stn_name) )); ?>">
                                <div class="table-out">
                                    <div class="table-in">
                                    <h5><?php echo $value['title'] ?></h5>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php endforeach ?>
            <div class="clear clearfix"></div>
        </div>

        <div class="clear"></div>
    </div>
</section>