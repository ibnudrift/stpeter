<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<header class="header sticky-top <?php if ($active_menu_pg == 'home/index'): ?>homepages<?php endif ?>">
    <div class="tops_heads_grey d-none d-sm-none d-md-none d-lg-block">
      <div class="container">
        <div class="row">
          <div class="col-md-50"></div>
          <div class="col-md-10">
            <div class="buttons-contactn-head">
              <a class="b_gold" href="<?php echo CHtml::normalizeUrl(array('/mass')); ?>"><i class="fa fa-pencil-alt"></i> &nbsp;Parishioner Registration</a>
              <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>"><i class="fa fa-phone"></i> &nbsp;Contact</a>
              <a class="b_red" href="<?php echo CHtml::normalizeUrl(array('/home/what_news')); ?>"><i class="fa fa-bell"></i> &nbsp;WHAT'S NEW</a>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear clearfix"></div>
      </div>
    </div>
    <div class="container d-none d-sm-none d-md-none d-lg-block">
      <div class="row">
        <div class="col-md-20">
          <div class="logo-headers">
            <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>logo-header.svg" alt="" class="img-fluid"></a>
          </div>
        </div>
        <div class="col-md-40">
          <div class="menu-headers text-right">
            <ul class="list-unstyled list-inline">
              <li class="list-inline-item <?php if ($this->id.'/'.$this->action->id == 'home/index'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
              <li class="list-inline-item <?php if ($this->id == 'about'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>">About STPCICP</a></li>
              <li class="list-inline-item <?php if ($this->id == 'sacrament'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/sacrament/index')); ?>">Sacraments</a></li>
              <li class="list-inline-item <?php if ($this->id == 'step'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/step/index')); ?>">Next Steps</a></li>
              <li class="list-inline-item <?php if ($this->id.'/'.$this->action->id == 'home/event' || $this->id.'/'.$this->action->id == 'home/eventDetail'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">Event / Bulletin</a></li>
              <li class="list-inline-item <?php if ($this->id.'/'.$this->action->id == 'home/faq'): ?>active<?php endif ?>"><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">FAQ</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="clear clearfix"></div>
    </div>

    <div class="d-block d-sm-block d-md-block d-lg-none">
      <nav class="navbar navbar-expand-md navbar-light bg-light">
          <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl ?>logo-header.svg" alt="" class="img-fluid"></a>
          <a href="<?php echo CHtml::normalizeUrl(array('/home/what_news')); ?>" class="btns_what"><img src="<?php echo $this->assetBaseurl.'lonceng-small.png'; ?>" class="img img-fluid"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
              <li class="active"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/about/index')); ?>">About STPCICP</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/sacrament/index')); ?>">Sacraments</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/step/index')); ?>">Next Steps</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">Event / Bulletin</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">FAQ</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact</a></li>

              <li><a href="<?php echo CHtml::normalizeUrl(array('/mass')); ?>">Parishioner Registration</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/what_news')); ?>">What`s New</a></li>
            </ul>

          </div>
        </nav>
      </div>
</header>

<script type="text/javascript">
  $(document).ready(function(){
      // $('.nl_popup a').live('hover', function(){
      //     $('.popup_carts_header').fadeIn();
      // });
      // $('.popup_carts_header').live('mouseleave', function(){
      //   setTimeout(function(){ 
      //       $('.popup_carts_header').fadeOut();
      //   }, 500);
      // });
  });
</script>
