<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<!-- Start fcs -->
<div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
    <div id="myCarousel_home" class="carousel slide" data-ride="carousel">
        <?php
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->group = 't.id';
        $criteria->order = 't.id ASC';
        $slide = Slide::model()->with(array('description'))->findAll($criteria);
        ?>
        <div class="carousel-inner">
            <?php foreach ($slide as $key => $value): ?>
            <div class="carousel-item <?php if ($key == 0): ?>active<?php endif ?>">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,869, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->title ?>" class="img-fluid d-none d-sm-block">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->title ?>" class="img-fluid d-block d-sm-none">
                
                <div class="carousel-caption d-block d-md-block">
                    <div class="bxsl_tx_fcs">
                        <h4><?php echo $value->description->title; ?></h4>
                        <p><?php echo $value->description->content; ?></p>
                        <div class="clear height-20"></div>
                        <?php if ($value->description->hide_button != 1): ?>
                        <a href="http://expatcatholicparish.org/about/index" class="btn customs_btn_fcs"><?php echo $value->description->url_teks; ?></a>
                        <?php endif ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>

        <!-- <div class="blocks_in_tfcs hide hidden">
            <div class="prelatife container">
                <div class="ins_tFcs">
                    <h4>You are in your Father's House</h4>
                    <p>St. Peter Canisius International Catholic Parish</p>
                    <div class="clear height-20"></div>
                    <a href="#" class="btn customs_btn_fcs">COME ON IN</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div> -->
        <div class="clear"></div>
    </div>

    <div class="clear"></div>
</div>
<!-- End fcs -->

<?php echo $content; ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<script type="text/javascript">
    $(document).ready(function(){
        
        if ($(window).width() > 768) {
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-none.d-sm-block').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }

    })
</script>
<?php $this->endContent(); ?>