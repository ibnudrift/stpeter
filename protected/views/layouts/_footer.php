
<section class="block-top-footer content-col-7">
	<div class="container back-content">
		<div class="text-center contents">
			<h3>Would like to know more?</h3>
			<p>GET CONNECTED WITH OUR SOCIAL MEDIA</p>
			<div class="socialsc_media">
				<a target="_blank" href="<?php echo $this->setting['url_instagram']; ?>"><i class="fab fa-instagram"></i></a>
				&nbsp;&nbsp;
				<a target="_blank" href="<?php echo $this->setting['url_facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
				&nbsp;&nbsp;
				<a target="_blank" href="<?php echo $this->setting['url_linkedin']; ?>"><i class="fab fa-linkedin-in"></i></a>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<section class="middles-footersn-whitegrey">
	<div class="container">
		<div class="row">
			<div class="col-md-38 back-white">
				<div class="row">
					<div class="col-md-20">
						<div class="contents">
							<h6 class="sub-titles">STPCICP</h6>
							<ul class="list-unstyled">
								<li><a href="<?php echo CHtml::normalizeUrl(array('/sacrament/index')); ?>">Sacraments</a></li>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/home/event')); ?>">Event / Bulletin</a></li>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">Faq</a></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
					<div class="col-md-20">
						<div class="contents">
							<h6 class="sub-titles">Next Step</h6>
							<ul class="list-unstyled">
								<li><a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=>'Church')); ?>">Church</a></li>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=>'Family')); ?>">Familiy</a></li>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/step/detail', 'pagename'=>'Youth')); ?>">Youth</a></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
					<div class="col-md-20">
						<div class="contents">
							<h6 class="sub-titles">Useful</h6>
							<ul class="list-unstyled">
								<li><a href="<?php echo CHtml::normalizeUrl(array('/home/faq')); ?>">FAQ</a></li>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact</a></li>
							</ul>
							<div class="clear"></div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-22 back-grey">
				<div class="contents pl-30">
					<h6 class="sub-titles">Get In Touch With Us</h6>
					<div class="clear"></div>
					<dl class="row">
					  <dt class="col-sm-5"><i class="fas fa-phone"></i></dt>
					  <dd class="col-sm-55"><?php echo $this->setting['contact_phone'] ?>, +62 21 39899623</dd>
					</dl>
					<dl class="row">
					  <dt class="col-sm-5"><i class="fas fa-paper-plane"></i></dt>
					  <dd class="col-sm-55"><a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></dd>
					</dl>
					<div class="clear"></div>
					<div class="socialsc_media text-left">
						<a target="_blank" href="<?php echo $this->setting['url_instagram']; ?>"><i class="fab fa-instagram"></i></a>
						&nbsp;&nbsp;
						<a target="_blank" href="<?php echo $this->setting['url_facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
						&nbsp;&nbsp;
						<a target="_blank" href="<?php echo $this->setting['url_linkedin']; ?>"><i class="fab fa-linkedin-in"></i></a>
						<div class="clear"></div>
					</div>

					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- footer -->
<footer class="footer">
	<div class="container">
		<div class="logo-footers d-block mx-auto text-center">
			<a href="#">
				<img src="<?php echo $this->assetBaseurl ?>logo-footer.svg" alt="" class="img-fluid d-block mx-auto">
			</a>
		</div>
		<p>Copyright &copy; St. Peter Canisius International Catholic Parish 2018.</p>
		<div class="clear"></div>
	</div>
</footer>
<!-- akhir footer -->

