<?php

/**
 * This is the model class for table "tb_loggers".
 *
 * The followings are the available columns in table 'tb_loggers':
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $urls
 * @property string $date_input
 * @property integer $sorts
 */
class Loggers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Loggers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_loggers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sorts', 'numerical', 'integerOnly'=>true),
			array('name, type', 'length', 'max'=>225),
			array('urls, date_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, type, urls, date_input, sorts', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'type' => 'Type',
			'urls' => 'Urls',
			'date_input' => 'Date Input',
			'sorts' => 'Sorts',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('urls',$this->urls,true);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('sorts',$this->sorts);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}