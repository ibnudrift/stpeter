<?php

/**
 * This is the model class for table "tb_mass_reg".
 *
 * The followings are the available columns in table 'tb_mass_reg':
 * @property string $id
 * @property string $name
 * @property string $date_birth
 * @property string $nationality
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $date_input
 * @property string $register_code
 * @property string $date_end_event
 * @property string $url_qrcode
 * @property string $age
 * @property string $event_id
 * @property string $event_name
 */
class MassReg extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MassReg the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_mass_reg';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, nationality, phone, email, register_code', 'length', 'max'=>225),
			array('date_birth, address, date_input, date_end_event, url_qrcode, age, event_id, event_name, parish_belong, length_stay_jakarta, family_member', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, date_birth, nationality, address, phone, email, date_input, register_code, date_end_event, url_qrcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'date_birth' => 'Date Birth',
			'nationality' => 'Nationality',
			'address' => 'Address',
			'phone' => 'Phone',
			'email' => 'Email',
			'date_input' => 'Date Input',
			'register_code' => 'Register Code',
			'date_end_event' => 'Date End Event',
			'url_qrcode' => 'URL QrCode',
			'hadir' => 'Hadir',
			'age' => 'Age',
			'event_id' => 'Event ID',
			'event_name' => 'Event Name',
			'parish_belong' => 'Which parish you belong to?',
			'length_stay_jakarta' => 'Length of Stay in Jakarta (specify month/ year)',
			'family_member' => 'Please specify the name of family members staying in the same house and their age. (name/age, name/age, name/age)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_birth',$this->date_birth,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		// $criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('register_code',$this->register_code,true);
		$criteria->compare('date_end_event',$this->date_end_event,true);
		$criteria->compare('url_qrcode',$this->url_qrcode,true);
		$criteria->compare('hadir',$this->hadir,true);
		$criteria->compare('age',$this->age,true);
		$criteria->compare('event_id',$this->event_id,true);
		$criteria->compare('event_name',$this->event_name,true);
		
		$criteria->compare('parish_belong',$this->parish_belong,true);
		$criteria->compare('length_stay_jakarta',$this->length_stay_jakarta,true);
		$criteria->compare('family_member',$this->family_member,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function totalPeserta($event_id)
	{
		$n_total = MassReg::model()->findAll('event_id = :evn_id', array(':evn_id'=>$event_id));

		return count($n_total);
	}

	public function totalPeserta_hadir($event_id)
	{
		$n_total = MassReg::model()->findAll('event_id = :evn_id and hadir = "1"', array(':evn_id'=>$event_id));
		return count($n_total);
	}

	public function totalPeserta_belum($event_id)
	{
		$n_total = MassReg::model()->findAll('event_id = :evn_id and hadir = "0"', array(':evn_id'=>$event_id));
		return count($n_total);
	}

}