<?php

/**
 * This is the model class for table "tb_mass".
 *
 * The followings are the available columns in table 'tb_mass':
 * @property string $id
 * @property string $date
 * @property string $name_sunday
 * @property string $chruch_name
 * @property string $chruch_address
 * @property string $city
 * @property integer $sorts
 */
class Mass_m extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mass_m the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_mass';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sorts', 'numerical', 'integerOnly'=>true),
			array('name_sunday, chruch_name, city', 'length', 'max'=>225),
			array('date, chruch_address', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, name_sunday, chruch_name, chruch_address, city, sorts', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'name_sunday' => 'Name Sunday',
			'chruch_name' => 'Chruch Name',
			'chruch_address' => 'Chruch Address',
			'city' => 'City',
			'sorts' => 'Sorts',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('name_sunday',$this->name_sunday,true);
		$criteria->compare('chruch_name',$this->chruch_name,true);
		$criteria->compare('chruch_address',$this->chruch_address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('sorts',$this->sorts);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}