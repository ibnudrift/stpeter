<?php

class MassRegController extends ControllerAdmin
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layoutsAdmin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			array('admin.filter.AuthFilter'),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			(!Yii::app()->user->isGuest)?
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array(Yii::app()->user->name),
			):array(),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MassReg;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MassReg']))
		{
			$model->attributes=$_POST['MassReg'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("MassRegController Create $model->id");
					Yii::app()->user->setFlash('success','Data has been inserted');
				    $transaction->commit();
					$this->redirect(array('index'));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MassReg']))
		{
			$model->attributes=$_POST['MassReg'];
			if($model->validate()){
				$transaction=$model->dbConnection->beginTransaction();
				try
				{
					$model->save();
					Log::createLog("MassRegController Update $model->id");
					Yii::app()->user->setFlash('success','Data Edited');
				    $transaction->commit();
					$this->redirect(array('index', 'event_id'=> $model->event_id));
				}
				catch(Exception $ce)
				{
				    $transaction->rollback();
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$mod_par = $this->loadModel($id);
			$events_id = $mod_par->event_id;
			$mod_par->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			// if(!isset($_GET['ajax']))
			$this->redirect( array('index', 'event_id'=> $model->event_id) );
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new MassReg('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MassReg']))
			$model->attributes=$_GET['MassReg'];

		$model->event_id = $_GET['event_id'];

		$dataProvider=new CActiveDataProvider('MassReg');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	public function actionDownload_excel()
	{
		$event_id = $_GET['event_id'];
		$alls_data = MassReg::model()->findAll('event_id = :evn_id', array(':evn_id'=>$event_id));
		$find_data = MassReg::model()->find('event_id = :evn_id', array(':evn_id'=>$event_id));

		$name_excel = 'download_'.$find_data->event_name.'.xls';

		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=". $name_excel);

		$str = '<table border="1">
					<tr>
						<th>No</th>
						<th>Name</th>
						<th>Age</th>
						<th>Address</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Nationality</th>
						<th>Event Name</th>

						<th>Which parish you belong to?</th>
						<th>LENGTH OF STAY IN JAKARTA</th>
						<th>SPECIFY THE NAME / AGE OF FAMILY MEMBERS STAYING IN THE SAME HOUSE</th>
					</tr>';
					// <th>Date Of Birth</th>
					// <th>Presence</th>
		$nom = 1;
		foreach ($alls_data as $key => $value) {
			$str .= '<tr>';
				$str .= '<td>'. $nom .'</td>';
				$str .= '<td>'. $value->name .'</td>';
				// $str .= '<td>'. $value->date_birth .'</td>';
				$str .= '<td>'. $value->age .'</td>';
				$str .= '<td>'. $value->address .'</td>';
				$str .= '<td>'. $value->email .'</td>';
				$str .= '<td>'. $value->phone .'</td>';
				$str .= '<td>'. $value->nationality .'</td>';
				
				// if ($value->hadir == 1) {
				// 	$str .= '<td>Attend</td>';
				// } else {
				// 	$str .= '<td>Not Present</td>';
				// }
				
				$str .= '<td>'. $value->event_name .'</td>';

				$str .= '<td>'. $value->parish_belong .'</td>';
				$str .= '<td>'. $value->length_stay_jakarta .'</td>';
				$str .= '<td>'. $value->family_member .'</td>';
			$str .= '</tr>';
			$nom++;
		}
		$str .= '</table>';

		echo $str;
		exit;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MassReg::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mass-reg-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
