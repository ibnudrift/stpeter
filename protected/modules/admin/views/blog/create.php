<?php
$this->breadcrumbs=array(
	'Event & Bulletin'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-book',
	'title'=>'Event & Bulletin',
	'subtitle'=>'Data Event & Bulletin',
);

$this->menu=array(
	array('label'=>'List Event & Bulletin', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc, 'modelImage'=>$modelImage)); ?>