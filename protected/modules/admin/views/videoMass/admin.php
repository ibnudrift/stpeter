<?php
$this->breadcrumbs=array(
	'Video Masses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List VideoMass','url'=>array('index')),
	array('label'=>'Add VideoMass','url'=>array('create')),
);
?>

<h1>Manage Video Masses</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'video-mass-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'url_video',
		'date_input',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
