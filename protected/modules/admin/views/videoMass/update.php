<?php
$this->breadcrumbs=array(
	'Video Mass'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Video Mass',
	'subtitle'=>'Edit Video Mass',
);

$this->menu=array(
	array('label'=>'List Video Mass', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Video Mass', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Video Mass', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>