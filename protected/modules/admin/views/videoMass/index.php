<?php
$this->breadcrumbs=array(
	'Video Mass',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Video Mass',
	'subtitle'=>'Data Video Mass',
);

$this->menu=array(
	array('label'=>'Add Video Mass', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Video Mass</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'video-mass-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'nama',
		'url_video',
		// 'date_input',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
