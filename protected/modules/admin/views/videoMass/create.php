<?php
$this->breadcrumbs=array(
	'Video Mass'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Video Mass',
	'subtitle'=>'Add Video Mass',
);

$this->menu=array(
	array('label'=>'List Video Mass', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>