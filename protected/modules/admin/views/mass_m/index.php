<?php
$this->breadcrumbs=array(
	'Mass Event',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Event',
	'subtitle'=>'Data Mass Event',
);

$this->menu=array(
	array('label'=>'Add Mass Event', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Mass Event</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'mass-m-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'date',
		'name_sunday',
		// 'chruch_name',
		'chruch_address',
		// 'city',
		array(
			'header' => 'Total Peserta',
			'type' => 'raw',
			'value' => 'MassReg::model()->totalPeserta($data->id)',
		),
		array(
			'header' => 'Total Hadir',
			'type' => 'raw',
			'value' => 'MassReg::model()->totalPeserta_hadir($data->id)',
		),
		array(
			'header' => 'Total Tidak Hadir',
			'type' => 'raw',
			'value' => 'MassReg::model()->totalPeserta_belum($data->id)',
		),
		
		/*
		'sorts',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {view_list}',
			'buttons'=>array(
				'view_list' => array(
				    'label'=>'<i class="fa fa-arrow-right"></i>',  
				    'url'=>'CHtml::normalizeUrl(array("/admin/massReg/index", "event_id" => $data->id))',
				),
			),

		),
	),
)); ?>

<script type="text/javascript">
	jQuery(function($){


	});
</script>