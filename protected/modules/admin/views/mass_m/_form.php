<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'mass-m-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Mass_m</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'date',array('class'=>'span5 datepicker', 'required'=>'required')); ?>

	<?php echo $form->textFieldRow($model,'name_sunday',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'chruch_name',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'chruch_address',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'city',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textFieldRow($model,'sorts',array('class'=>'span5')); ?>

		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
