<?php
$this->breadcrumbs=array(
	'Mass Event'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Event',
	'subtitle'=>'Edit Mass Event',
);

$this->menu=array(
	array('label'=>'List Mass Event', 'icon'=>'th-list','url'=>array('index')),
	// array('label'=>'Add Mass Event', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Mass Event', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>