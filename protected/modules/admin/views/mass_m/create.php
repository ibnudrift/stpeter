<?php
$this->breadcrumbs=array(
	'Mass Event'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Event',
	'subtitle'=>'Add Mass Event',
);

$this->menu=array(
	array('label'=>'List Mass Event', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>