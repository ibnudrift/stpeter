<?php
$this->breadcrumbs=array(
	'Mass Ms'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Mass_m','url'=>array('index')),
	array('label'=>'Add Mass_m','url'=>array('create')),
);
?>

<h1>Manage Mass Ms</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'mass-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date',
		'name_sunday',
		'chruch_name',
		'chruch_address',
		'city',
		/*
		'sorts',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
