<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_sunday')); ?>:</b>
	<?php echo CHtml::encode($data->name_sunday); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chruch_name')); ?>:</b>
	<?php echo CHtml::encode($data->chruch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chruch_address')); ?>:</b>
	<?php echo CHtml::encode($data->chruch_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sorts')); ?>:</b>
	<?php echo CHtml::encode($data->sorts); ?>
	<br />


</div>