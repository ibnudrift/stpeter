<?php
$this->breadcrumbs=array(
	'Mass Ms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Mass_m', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Mass_m', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit Mass_m', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Mass_m', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Mass_m #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'name_sunday',
		'chruch_name',
		'chruch_address',
		'city',
		'sorts',
	),
)); ?>
