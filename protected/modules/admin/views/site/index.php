<?php
$this->breadcrumbs=array(
    'Dashboard',
);
?>
<?php
$session = new CHttpSession;
$session->open();
$login_admin = $session['login'];
?>
<div class="pageheader">
    
    <div class="pageicon"><span class="fa fa-laptop"></span></div>
    <div class="pagetitle">
        <h5>All Features Summary</h5>
        <h1>Dashboard</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <div class="row-fluid">
            <div id="dashboard-left" class="span8">
                    <h5 class="subtitle">Menu</h5>
                
                    <ul class="shortcuts">
                        <?php if ($login_admin['type'] == 'root'): ?>
                        <li class="products">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')); ?>">
                                <i class="icon-cms fa fa-image"></i>
                                <span class="shortcuts-label">Slide</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/static/about')); ?>">
                                <i class="icon-cms fa fa-info"></i>
                                <span class="shortcuts-label">About Us</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'sacraments', 'title'=>'Sacraments & Services')); ?>">
                                <i class="icon-cms fa fa-delicious"></i>
                                <span class="shortcuts-label">Sacraments</span>
                            </a>
                        </li>

                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/static/next')); ?>">
                                <i class="icon-cms fa fa-paperclip"></i>
                                <span class="shortcuts-label">Next Steps</span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/blog/index')); ?>">
                                <i class="icon-cms fa fa-inbox"></i>
                                <span class="shortcuts-label">Events</span>
                            </a>
                        </li>
                        <?php if ($login_admin['type'] == 'root'): ?>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/static/index', 'page'=>'faq', 'title'=>'Faq')); ?>">
                                <i class="icon-cms fa fa-flag"></i>
                                <span class="shortcuts-label">Faq</span>
                            </a>
                        </li>

                        <?php /*
                        <li class="events">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/product/index')); ?>">
                                <i class="icon-cms fa fa-tag"></i>
                                <span class="shortcuts-label">Products</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/healty/index')); ?>">
                                <i class="icon-cms fa fa-heart"></i>
                                <span class="shortcuts-label">Healty</span>
                            </a>
                        </li>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/gema/index')); ?>">
                                <i class="icon-cms fa fa-group"></i>
                                <span class="shortcuts-label">ge-ma</span>
                            </a>
                        </li>
                        */ ?>
                        <li class="archive">
                            <a href="<?php echo CHtml::normalizeUrl(array('/admin/static/contact')); ?>">
                                <i class="icon-cms fa fa-phone"></i>
                                <span class="shortcuts-label">Contact Us</span>
                            </a>
                        </li>
                        <?php endif ?>

                </ul>

            </div> <!-- span-8 -->
            
            <div id="dashboard-right" class="span4">

                <h5 class="subtitle">Announcements</h5>
                
                <div class="divider15"></div>
                
                <div class="alert alert-block">
                      <button data-dismiss="alert" class="close" type="button">&times;</button>
                      <h4>Warning!</h4>
                      <p style="margin: 8px 0">Download User Guide <a href="<?php echo Yii::app()->baseUrl.'/images/ST-Peter-Website-Guide.pdf' ?>">here</a> </p>
                </div><!--alert-->
                
                <br />
                
                
                <br />
                                        
            </div><!--span4-->
        </div><!--row-fluid-->
        
        <div class="footer">
            <div class="footer-left">
                <span>Copyright &copy; <?php echo date('Y'); ?> by <?php echo Yii::app()->name ?>.</span>
            </div>
            <div class="footer-right">
                <span>All Rights Reserved. Developed By <a target="_blank" href="http://markdesign.net">Markdesign</a></span>
            </div>
        </div><!--footer-->
        
    </div><!--maincontentinner-->
</div><!--maincontent-->