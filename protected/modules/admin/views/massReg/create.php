<?php
$this->breadcrumbs=array(
	'Mass Register List'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Register List',
	'subtitle'=>'Add Mass Register List',
);

$this->menu=array(
	array('label'=>'List Mass Register List', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>