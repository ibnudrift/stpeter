<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'mass-reg-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data MassReg</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>225)); ?>

	<?php // echo $form->textFieldRow($model,'date_birth',array('class'=>'span5 datepicker')); ?>

	<?php echo $form->textAreaRow($model,'address',array('rows'=>3, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'age',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'nationality',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'parish_belong',array('class'=>'span5','maxlength'=>225)); ?>
	<?php echo $form->textFieldRow($model,'length_stay_jakarta',array('class'=>'span5','maxlength'=>225)); ?>
	<?php echo $form->textAreaRow($model,'family_member',array('rows'=>3, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textFieldRow($model,'date_input',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'register_code',array('class'=>'span5','readonly'=>'readonly')); ?>

	<?php echo $form->dropDownListRow($model, 'hadir', array(
        		'1'=>'Hadir',
        		'0'=>'Tidak Hadir',
        	), array('readonly'=>'readonly')); ?>

	<div class="control-group">
		<label class="control-label" for="MassReg_url_qrcode">URL QrCode</label>
		<div class="controls">
			<div class="thumbnail_qr">
				<img src="<?php echo $model->url_qrcode ?>" alt="" style="max-width: 140px;">
			</div>
			<a target="_blank" href="<?php echo $model->url_qrcode ?>" class="btn btn-link">Link QrCode</a>
		</div>
	</div>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'url'=>CHtml::normalizeUrl(array('index', 'event_id'=> $model->event_id)),
		'label'=>'Batal',
	)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
