<?php
$this->breadcrumbs=array(
	'Mass Register List',
);

$n_parent = Mass_m::model()->findByPk($_GET['event_id']);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Register List',
	'subtitle'=>'Data Mass Register List > <small>'. $n_parent->name_sunday .' - '. $n_parent->date.'</small>',
);

$this->menu=array(
	array( 'label'=>'Download Excel', 'icon'=>'download','url'=>array('download_excel', 'event_id'=> $_GET['event_id']), 'htmlOptions'=>array('target'=>'_blank') ),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>
<?php endif; ?>


<h1>Mass Register List</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'mass-reg-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'name',
		'phone',
		'email',
		'age',
		'nationality',
		// 'date_input',
		// array(
		// 	'header' => 'Kehadiran',
		// 	'type' => 'raw',
		// 	'value' => '($data->hadir == "1")? "Hadir": "Tidak Hadir"',
		// ),

		/*
		'address',
		'date_birth',
		'register_code',
		'date_end_event',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
