<?php
$this->breadcrumbs=array(
	'Mass Regs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MassReg','url'=>array('index')),
	array('label'=>'Add MassReg','url'=>array('create')),
);
?>

<h1>Manage Mass Regs</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'mass-reg-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'date_birth',
		'nationality',
		'address',
		'phone',
		/*
		'email',
		'date_input',
		'register_code',
		'date_end_event',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
