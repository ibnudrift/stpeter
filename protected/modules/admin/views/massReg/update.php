<?php
$this->breadcrumbs=array(
	'Mass Register List'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Mass Register List',
	'subtitle'=>'Edit Mass Register List',
);

$this->menu=array(
	array('label'=>'List Mass Register List', 'icon'=>'th-list','url'=>array('index', 'event_id'=> $model->event_id)),
	// array('label'=>'Add Mass Register List', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Mass Register List', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>