<?php 
	// $criteria = new CDbCriteria;
	// $criteria->with = array('description');
	// $criteria->addCondition('description.language_id = :language_id');
	// $criteria->params[':language_id'] = 2;
	// $criteria->addCondition('t.id = :id');
	// $criteria->params[':id'] = $_GET['category'];
	// $criteria->group = 't.id';
	// $criteria->order = 't.sort ASC';
	// $detailCategory = PrdCategory::model()->find($criteria);
	// $titles_subm = $detailCategory->description->name;
?>
<?php
$strn = '';
if ($_GET['category'] == 1) {
	$strn = 'Bulletin';
} else {
	$strn = 'Missal';
}

$this->breadcrumbs=array(
	$strn,
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>$strn,
	'subtitle'=>'Data '. $strn,
);

$this->menu=array(
	array('label'=>'Add '. $strn, 'icon'=>'plus-sign','url'=>array('create', 'category'=>$_GET['category'])),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<div class="row-fluid">
	<div class="span12">
<h1>PDF</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'bank-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		// 'ib_bank',
		'nama',
		// 'sort',
		'file',
		array(
			'name'=>'status',
			'filter'=>array(
				'0'=>'Non Active',
				'1'=>'Active',
			),
			'type'=>'raw',
			'value'=>'($data->status == "1") ? "Published" : "Unpublished"',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
			'deleteButtonUrl'=>'CHtml::normalizeUrl(array("delete", "id"=>$data->id, "category"=>"'.$_GET['category'].'"))',
			'updateButtonUrl'=>'CHtml::normalizeUrl(array("update", "id"=>$data->id, "category"=>"'.$_GET['category'].'"))',
		),
	),
)); ?>
</div>
</div>