<?php 
	// $criteria = new CDbCriteria;
	// $criteria->with = array('description');
	// $criteria->addCondition('description.language_id = :language_id');
	// $criteria->params[':language_id'] = 2;
	// $criteria->addCondition('t.id = :id');
	// $criteria->params[':id'] = $_GET['category'];
	// $criteria->group = 't.id';
	// $criteria->order = 't.sort ASC';
	// $detailCategory = PrdCategory::model()->find($criteria);
	// $titles_subm = $detailCategory->description->name;
?>
<?php
$strn = '';
if ($_GET['category'] == 1) {
	$strn = 'Bulletin';
} else {
	$strn = 'Missal';
}

$this->breadcrumbs=array(
	$strn=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>$strn,
	'subtitle'=>'Edit '.$strn,
);

$this->menu=array(
	array('label'=>'List '. $strn, 'icon'=>'th-list','url'=>array('index', 'category'=>$_GET['category'])),
	array('label'=>'Add '. $strn, 'icon'=>'plus-sign','url'=>array('create', 'category'=>$_GET['category'])),
	// array('label'=>'View '. $strn, 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>