<?php

class MassController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	// mass register
	public function actionIndex()
	{
		$this->pageTitle = 'Mass Registration - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$model = new MassReg;
		$model->scenario = 'insert';

		if(isset($_POST['MassReg']))
		{
			$model->attributes=$_POST['MassReg'];

	        $secret_key = "6LcoCugUAAAAAPLnU9cClliDeBZiXWQg686hoU0P";
	        $ch = curl_init("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);   
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		$response = curl_exec($ch);
    		curl_close($ch);
    		

	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          	$model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
	        }else{

	        	// get event data
				$criteria = new CDbCriteria;
				$criteria->order = 't.id DESC';
				$criteria->limit = 1;
				$q_event = Mass_m::model()->find($criteria);

	        	// check duplicate
	        	$f_member = MassReg::model()->find('email = :email and event_id = :evn_id', array(':email'=>$model->email, ':evn_id'=> $q_event->id));
	        	if ( $f_member !== null ) {
	        		$model->addError('email', 'Sorry, You are has been registered.');
	        	}

	        	// filter age
				$from = new DateTime($model->date_birth);
				$to   = new DateTime('today');
				$ages = $from->diff($to)->y;

	        	// if ($ages < 18 or $ages > 45) {
	        	// 	$model->addError('date_birth', 'Sorry, Regisration is Denied.');
	        	// }

				if(!$model->hasErrors() && $model->validate())
				{
					$seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
					shuffle($seed); // probably optional since array_is randomized; this may be redundant
					$rand = '';
					foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];

					$model->register_code = 'ST-PTR-'.date('Ym').'-'.$rand.'-'.rand(10, 1000);
					$model->date_input = date("Y-m-d H:i:s");

					$model->age = $ages;

					$str_qr_gen = 'https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=';
					$baseUrl = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
					$model->url_qrcode = $str_qr_gen . $baseUrl . '/mass/scan?kode=' . $model->register_code;

					// get event data
					$criteria = new CDbCriteria;
					$criteria->order = 't.id DESC';
					$criteria->limit = 1;
					$q_event = Mass_m::model()->find($criteria);

					$model->event_id = $q_event->id;
					$model->event_name = $q_event->name_sunday;
					$model->save(false);

					// Get event mass Data
					$criteria = new CDbCriteria;
					$criteria->order = 't.id DESC';
					$criteria->limit = 1;
					$mass_data = Mass_m::model()->find($criteria);

					// config email
					$messaged = $this->renderPartial('//mail/mass_schedule',array(
						'model'=>$model,
						'mass_data' => $mass_data,
					),TRUE);

					$config = array(
						'to'=>array($model->email, $this->setting['email']),
						'bcc'=>array('ibnu@markdesign.net'),
						'subject'=>'['.Yii::app()->name.'] Mass Registration from '.$model->email,
						'message'=>$messaged,
					);

					if ($this->setting['contact_cc']) {
						$config['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) {
						$config['bcc'] = array($this->setting['contact_bcc']);
					}

					// kirim email
					Common::mail($config);

					Yii::app()->user->setFlash('success','Thank you for Mass Regitration. We will respond to you as soon as possible.');
					$this->redirect(array('registration_process', 'member_id'=> $model->id));
				}
			}

		}

		$this->render('index', array(	
			'model' => $model,
		));
	}

	// mass register success
	public function actionRegistration_process()
	{
		$this->pageTitle = 'Mass Registration - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$id = intval($_GET['member_id']);
		$model = MassReg::model()->findByPk($id);

		$this->render('regis_success', array(
			'model' => $model,
		));
	}

	// mass scan
	public function actionScan()
	{
		$this->pageTitle = 'Mass Registration - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$criteria = new CDbCriteria;
		$criteria->order = 't.id DESC';
		$criteria->limit = 1;
		$event_active = Mass_m::model()->find($criteria);

		$error = false;

		if (isset($_POST['MassReg'])) {
			$emails = $_POST['MassReg']['email'];

			$data = MassReg::model()->find('email = :email and event_id = :evn_id', array(':email'=> $emails, ':evn_id'=> $event_active->id ));
			if ($data === null) {
				$error = true;
			}else{
				$this->render('kode_scan', array(
					'data' => $data,
				));
			}
			
		}

		if (isset($_GET['kode']) && $_GET['kode']) {
			$kodes = $_GET['kode'];
			$data = MassReg::model()->find('register_code = :kode and event_id = :evn_id', array(':kode'=> $kodes, ':evn_id'=> $event_active->id ));
			
			if ($data === null) {
				$error = true;
			}else{
				$this->render('kode_scan', array(
					'data' => $data,
				));
			}
		}

		$this->render('scan', array(	
			'error' => $error,
		));
	}

	// mass process approve
	public function actionPresent_event()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 't.id DESC';
		$criteria->limit = 1;
		$event_active = Mass_m::model()->find($criteria);
		
		$error_code = false;

		if (isset($_POST['MassReg'])) {
			$emails = $_POST['MassReg']['email'];
			
			$post_kode = '';
			foreach ($_POST['MassReg']['verifyCode'] as $key_n => $values_n) {
				$post_kode .= trim($values_n);
			}	

			$data = MassReg::model()->find('email = :email and event_id = :evn_id', array(':email'=> $emails, ':evn_id'=> $event_active->id ));
			// verify code TRUE
			if ($post_kode == $this->setting['mass_kode_setup']) {
				// echo "<pre>"; print_r($_POST['MassReg']); exit;
				if ($data !== null) {
					$data->hadir = 1;
					$data->save(false);

					$this->redirect(array('scan_result', 'member_id'=> $data->id));
				}
			}else{
				$error_code = true;
				$this->render('kode_scan', array(	
					'error_code' => $error_code,
					'data' => $data,
				));
			}
		}

	}
	
	// mass scan result
	public function actionScan_result()
	{
		$this->pageTitle = 'Mass Registration - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		if (isset($_GET['member_id'])) {
			$ids = $_GET['member_id'];
			
			$data = MassReg::model()->findByPk($ids);

			$this->render('scan_result', array(	
				'data' => $data,
			));

		}else{
			$this->redirect(array('scan'));
		}
	}
	

}